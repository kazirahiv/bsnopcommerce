﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Plugins;
using Nop.Web.Framework.Infrastructure;
using Nop.Plugin.Widgets.ShopLocation.Components;

namespace Nop.Plugin.Widgets.ShopLocation
{
    public class ShopLocationPlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;

        public bool HideInWidgetList => false;

        public ShopLocationPlugin(ISettingService settingService, IWebHelper webHelper, ILocalizationService localizationService)
        {
            this._webHelper = webHelper;
            this._settingService = settingService;
            this._localizationService = localizationService;
        }

        public string GetWidgetViewComponentName(string widgetZone)
        {
            return WidgetsShopLocationViewComponent.ViewComponentName;
        }

        public Task<IList<string>> GetWidgetZonesAsync()
        {
            return Task.FromResult<IList<string>>(new List<string> { PublicWidgetZones.HomepageTop });
        }


        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/ShopLocation/Configure";
        }


        public override async Task InstallAsync()
        {
            var settings = new ShopLocationSettings()
            {
                UseSandbox = true,
                Message = "Hello World"
            };
            await _settingService.SaveSettingAsync(settings);

            await _localizationService.AddLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Widgets.ShopLocation.Address"] = "Shop Address",
                ["Plugins.Widgets.ShopLocation.Name"] = "Shop Name",
                ["Plugins.Widgets.ShopLocation.ShopId"] = "Shop Id",
            });

            await base.InstallAsync();
        }

        public override async Task UninstallAsync()
        {
            await _settingService.DeleteSettingAsync<ShopLocationSettings>();
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Widgets.ShopLocation");

            await base.UninstallAsync();
        }



    }
}
