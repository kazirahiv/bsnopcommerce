﻿using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models;
using Nop.Plugin.Widgets.ShopLocation.Domain;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the shop location model factory
    /// </summary>
    public partial interface IShopLocationModelFactory
    {
        Task<ShopLocationSearchModel> PrepareShopLocationSearchModelAsync(ShopLocationSearchModel searchModel);

        Task<ShopLocationListModel> PrepareShopLocationListModelAsync(ShopLocationSearchModel searchModel);

        Task<LocationModel> PrepareShopLocationModelAsync(LocationModel model,
            Location location, bool excludeProperties = false);

    }
}