﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models;
using Nop.Plugin.Widgets.ShopLocation.Domain;
using Nop.Plugin.Widgets.ShopLocation.Infrastructure.Mapper.Extensions;
using Nop.Plugin.Widgets.ShopLocation.Services;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the manufacturer model factory implementation
    /// </summary>
    public partial class ShopLocationModelFactory : IShopLocationModelFactory
    {
        #region Fields
        private readonly IShopLocationService _shopLocationService;
        #endregion

        #region Ctor

        public ShopLocationModelFactory(IShopLocationService shopLocationService)
        {
            _shopLocationService = shopLocationService;
        }

        #endregion


        #region Methods

        /// <summary>
        /// Prepare manufacturer search model
        /// </summary>
        /// <param name="searchModel">Manufacturer search model</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the manufacturer search model
        /// </returns>
        public virtual async Task<ShopLocationSearchModel> PrepareShopLocationSearchModelAsync(ShopLocationSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));
            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        public virtual async Task<ShopLocationListModel> PrepareShopLocationListModelAsync(ShopLocationSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get manufacturers
            var shopLocations = await _shopLocationService.GetAllLocationsPagedAsync();

            // prepare grid model
            var model = await new ShopLocationListModel().PrepareToGridAsync(searchModel, shopLocations, () =>
            {
                //fill in model values from the entity
                return shopLocations.Select(shopLocation =>
                {

                    var locationModel = new LocationModel()
                    {
                        Id = shopLocation.Id,
                        ShopId = shopLocation.ShopId,
                        Name = shopLocation.Name,
                        Address = shopLocation.Address
                    };


                    return locationModel;
                }).ToAsyncEnumerable();
            });

            return model;
        }

        /// <summary>
        /// Prepare location model
        /// </summary>
        /// <param name="model">Manufacturer model</param>
        /// <param name="manufacturer">Manufacturer</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the manufacturer model
        /// </returns>
        public virtual async Task<LocationModel> PrepareShopLocationModelAsync(LocationModel model, Location location, bool excludeProperties = false)
        {
            if(location == null)
            {
                location = new Location();
            }
            var mapped = location.ToModel<LocationModel>();
            return mapped;
        }

            #endregion
        }
}