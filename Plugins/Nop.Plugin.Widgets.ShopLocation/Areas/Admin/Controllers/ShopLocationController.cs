﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Services.Messages;
using Nop.Plugin.Widgets.ShopLocation.Services;
using Nop.Plugin.Widgets.ShopLocation.Domain;
using System.Linq;
using Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models;
using Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Factories;
using Nop.Web.Framework.Mvc.Filters;
using System;
using Nop.Plugin.Widgets.ShopLocation.Infrastructure.Mapper.Extensions;
using System.Collections.Generic;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Controllers
{
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class ShopLocationController : BasePluginController
    {

        #region Fields

        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IShopLocationService _shopLocationService;
        private readonly IShopLocationModelFactory _shopLocationModelFactory;

        #endregion

        #region Ctor

        public ShopLocationController(
            INotificationService notificationService,
            IPermissionService permissionService,
            IShopLocationService shopLocationService,
            IShopLocationModelFactory shopLocationModelFactory)
        {
            _notificationService = notificationService;
            _permissionService = permissionService;
            _shopLocationService = shopLocationService;
            _shopLocationModelFactory = shopLocationModelFactory;
        }

        #endregion


        #region List

        public virtual IActionResult Configure()
        {
            return RedirectToAction("List");
        }

        /// <returns>A task that represents the asynchronous operation</returns>


        public async Task<IActionResult> List()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();
            var model = await _shopLocationModelFactory.PrepareShopLocationSearchModelAsync(new ShopLocationSearchModel());
            return View("~/Plugins/Widgets.ShopLocation/Areas/Admin/Views/List.cshtml", model);
        }

        [HttpPost]
        public virtual async Task<IActionResult> List(ShopLocationSearchModel searchModel)
        {
            //prepare model
            var model = await _shopLocationModelFactory.PrepareShopLocationListModelAsync(searchModel);

            return Json(model);
        }

        #endregion


        #region Create / Edit / Delete

        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Create()
        {
            //prepare model
            var model = await _shopLocationModelFactory.PrepareShopLocationModelAsync(new LocationModel(), null);

            return View("~/Plugins/Widgets.ShopLocation/Areas/Admin/Views/Create.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Create(LocationModel model, bool continueEditing)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {
                var location = model.ToEntity<Location>();
                await _shopLocationService.InsertLocationAsync(location);

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = location.Id });
            }

            //prepare model
            model = await _shopLocationModelFactory.PrepareShopLocationModelAsync(model, null, true);

            //if we got this far, something failed, redisplay form
            return View("~/Plugins/Widgets.ShopLocation/Areas/Admin/Views/Create.cshtml", model);
        }


        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Edit(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            //try to get a location with the specified id
            var location = await _shopLocationService.GetByIdAsync(id);
            if (location == null)
                return RedirectToAction("List");

            //prepare model
            var model = await _shopLocationModelFactory.PrepareShopLocationModelAsync(null, location);

            return View("~/Plugins/Widgets.ShopLocation/Areas/Admin/Views/Edit.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Edit(LocationModel model, bool continueEditing)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            //try to get a location with the specified id
            var manufacturer = await _shopLocationService.GetByIdAsync(model.Id);
            if (manufacturer == null)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                manufacturer = model.ToEntity(manufacturer);
                await _shopLocationService.UpdateLocationAsync(manufacturer);
                _notificationService.SuccessNotification("Location Updated !");

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = manufacturer.Id });
            }

            //prepare model
            model = await _shopLocationModelFactory.PrepareShopLocationModelAsync(model, manufacturer, true);

            //if we got this far, something failed, redisplay form
            return View("~/Plugins/Widgets.ShopLocation/Areas/Admin/Views/Create.cshtml", model);
        }


        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Delete(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            //try to get a location with the specified id
            var location = await _shopLocationService.GetByIdAsync(id);
            if (location == null)
                return RedirectToAction("List");

            await _shopLocationService.DeleteLocationAsync(id);

            _notificationService.SuccessNotification("Location Deleted !");

            return RedirectToAction("List");
        }

        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> DeleteSelected(ICollection<int> selectedIds)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var locations = await _shopLocationService.GetLocationsByIdsAsync(selectedIds.ToArray());
                await _shopLocationService.DeleteShopLocationsAsync(locations);
            }

            return Json(new { Result = true });
        }

        #endregion


        #region Old

        [HttpGet]
        public async Task<JsonResult> GetAllLocation()
        {
            var data = await _shopLocationService.GetAllAsync();

            var locations = data.Select(s => new LocationModel
            {
                Id = s.Id,
                ShopId = s.ShopId,
                Address = s.Address,
                Name = s.Name
            }).ToList();


            return Json(new { data = locations });
        }


        [HttpPost]
        public async Task<IActionResult> Configure(LocationModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            var location = new Location() { ShopId = model.ShopId, Name = model.Name, Address = model.Address };
            await _shopLocationService.InsertLocationAsync(location);

            _notificationService.SuccessNotification("Saved !");
            return Ok();
        }



        [HttpGet]
        public async Task<IActionResult> DeleteLocation(int locId)
        {

            var result = await _shopLocationService.DeleteLocationAsync(locId);

            if(result > 0)
            {
                _notificationService.SuccessNotification("Deleted !");
            }
            else
            {
                _notificationService.ErrorNotification("Not Deleted !");
            }
            return Ok();
        }


        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> UpdateLocation(LocationModel model)
        {
            var result = await _shopLocationService.GetByIdAsync(model.Id);

            if(result != null)
            {
                result.Name = model.Name;
                result.ShopId = model.ShopId;
                result.Address = model.Address;
                await _shopLocationService.UpdateLocationAsync(result);
                _notificationService.SuccessNotification("Updated !");
                return Ok();
            }
            else
            {
                _notificationService.ErrorNotification("Not Deleted !");
                return NotFound();
            }
        }
        #endregion
    }
}
