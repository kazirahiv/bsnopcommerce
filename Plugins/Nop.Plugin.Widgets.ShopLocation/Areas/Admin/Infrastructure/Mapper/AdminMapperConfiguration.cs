﻿using Nop.Core.Infrastructure.Mapper;
using AutoMapper;
using Nop.Plugin.Widgets.ShopLocation.Domain;
using Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Infrastructure.Mapper
{
    class AdminMapperConfiguration : Profile, IOrderedMapperProfile
    {

        #region Ctor
        public AdminMapperConfiguration()
        {
            CreateLocationMaps();
        }

        #endregion




        #region Utilities

        protected virtual void CreateLocationMaps()
        {
            CreateMap<Location, LocationModel>();
            CreateMap<LocationModel, Location>();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion
    }
}
