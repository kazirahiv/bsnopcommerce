﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models
{

    /// <summary>
    /// Represents a location search model
    /// </summary>
    public partial record ShopLocationSearchModel : BaseSearchModel
    {

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Manufacturers.List.SearchManufacturerName")]
        public string SearchShopLocationName { get; set; }

        #endregion
    }
}
