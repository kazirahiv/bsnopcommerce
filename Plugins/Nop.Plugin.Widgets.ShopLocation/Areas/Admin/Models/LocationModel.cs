﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models
{
    public record LocationModel :  BaseNopEntityModel
    {
        [NopResourceDisplayName("Plugins.Widgets.ShopLocation.ShopId")]
        public string ShopId { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.ShopLocation.Name")]
        public string Name { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.ShopLocation.Address")]
        public string Address { get; set; }
    }
}
