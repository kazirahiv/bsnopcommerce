﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Widgets.ShopLocation.Areas.Admin.Models
{

    public partial record ShopLocationListModel : BasePagedListModel<LocationModel>
    {
    }
}
