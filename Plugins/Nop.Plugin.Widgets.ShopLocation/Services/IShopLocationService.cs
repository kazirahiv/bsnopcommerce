﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Widgets.ShopLocation.Domain;

namespace Nop.Plugin.Widgets.ShopLocation.Services
{
    /// <summary>
    /// Represents service shipping by weight service
    /// </summary>
    public partial interface IShopLocationService
    {
        Task<List<Location>> GetAllAsync();
        Task InsertLocationAsync(Location location);
        Task UpdateLocationAsync(Location location);
        Task<int> DeleteLocationAsync(int id);
        Task DeleteShopLocationsAsync(IList<Location> locations);
        Task<Location> GetByIdAsync(int id);
        Task<IList<Location>> GetLocationsByIdsAsync(int[] locationIds);
        Task<IPagedList<Location>> GetAllLocationsPagedAsync(int pageIndex = 0,
            int pageSize = int.MaxValue);
    }
}
