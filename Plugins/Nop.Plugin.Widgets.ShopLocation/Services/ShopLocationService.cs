﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Core.Caching;
using Nop.Data;
using Nop.Plugin.Widgets.ShopLocation.Domain;
using Nop.Plugin.Widgets.ShopLocation.Services;

namespace Nop.Plugin.Shipping.FixedByWeightByTotal.Services
{
    /// <summary>
    /// Represents service shipping by weight service implementation
    /// </summary>
    public partial class ShopLocationService : IShopLocationService
    {

        #region Fields

        private readonly IRepository<Location> _locationRepository;

        #endregion

        #region Ctor

        public ShopLocationService(IRepository<Location> locationRepository)
        {
            _locationRepository = locationRepository;
        }

        #endregion

        #region Methods


        public virtual async Task<List<Location>> GetAllAsync()
        {
            var result = await _locationRepository.GetAllAsync(query =>
            {
                return from sbw in query select sbw;
            });

            var records = result.ToList();

            return records;
        }

        public virtual async Task<IPagedList<Location>> GetAllLocationsPagedAsync(int pageIndex = 0,
            int pageSize = int.MaxValue)
        {

            return await _locationRepository.GetAllPagedAsync(async query =>
            {
                return query.OrderBy(s=>s.Id);
            }, pageIndex, pageSize);
        }


        public virtual async Task<IList<Location>> GetLocationsByIdsAsync(int[] locationIds)
        {
            return await _locationRepository.GetByIdsAsync(locationIds, includeDeleted: false);
        }

        public virtual async Task DeleteShopLocationsAsync(IList<Location> locations)
        {
            await _locationRepository.DeleteAsync(locations);
        }





        public virtual async Task<Location> GetByIdAsync(int id)
        {
            var record = await _locationRepository.GetByIdAsync(id);

            return record;
        }

        public virtual async Task InsertLocationAsync(Location location)
        {
            await _locationRepository.InsertAsync(location, false);
        }

        public virtual async Task UpdateLocationAsync(Location location)
        {
            await _locationRepository.UpdateAsync(location);
        }

        public virtual async Task<int> DeleteLocationAsync(int id)
        {
            var result = await _locationRepository.DeleteAsync(s => s.Id == id);
            return result;
        }

        #endregion
    }
}
