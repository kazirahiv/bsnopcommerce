﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.ShopLocation.Models
{
    public record PublicInfoModel : BaseNopModel
    {
        public int Id { get; set; }
        public string ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
