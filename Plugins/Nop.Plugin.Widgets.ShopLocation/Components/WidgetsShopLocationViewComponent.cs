﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Web.Framework.Components;
using Nop.Plugin.Widgets.ShopLocation.Services;
using System.Linq;
using Nop.Plugin.Widgets.ShopLocation.Models;

namespace Nop.Plugin.Widgets.ShopLocation.Components
{
    [ViewComponent(Name = "WidgetsShopLocation")]
    public class WidgetsShopLocationViewComponent : NopViewComponent
    {
        public static string ViewComponentName => "WidgetsShopLocation";
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly IShopLocationService _shopLocationService;


        public WidgetsShopLocationViewComponent(IStoreContext storeContext, ISettingService settingService, IShopLocationService shopLocationService)
        {
            this._storeContext = storeContext;
            this._settingService = settingService;
            _shopLocationService = shopLocationService;
        }

        public async Task<IViewComponentResult> InvokeAsync(string widgetZone, object additionalData)
        {
            var data = await _shopLocationService.GetAllAsync();

            var locations = data.Select(s => new PublicInfoModel
            {
                ShopId = s.ShopId,
                Address = s.Address,
                Name = s.Name
            }).ToList();

            return View("~/Plugins/Widgets.ShopLocation/Views/PublicInfo.cshtml", locations);
        }


    }
}
