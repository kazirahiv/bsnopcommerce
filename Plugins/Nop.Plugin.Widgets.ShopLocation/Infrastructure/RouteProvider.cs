﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Widgets.ShopLocation.Infrastructure
{
    class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IEndpointRouteBuilder endpointRouteBuilder)
        {
            endpointRouteBuilder.MapControllerRoute(ShopLocationDefaults.ConfigurationRouteName, "Plugins/ShopLocation/Configure",
                new { controller = "ShopLocation", action = "Configure" });
            //areas
            endpointRouteBuilder.MapControllerRoute(name: "areaRoute",
                pattern: $"{{area:exists}}/{{controller=ShopLocation}}/{{action=Index}}/{{id?}}");
        }

        public int Priority => 0;
    }
}
