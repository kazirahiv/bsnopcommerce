﻿using FluentMigrator;
using Nop.Data.Migrations;
using Nop.Plugin.Widgets.ShopLocation.Domain;

namespace Nop.Plugin.Widgets.ShopLocation.Data
{
    [SkipMigrationOnUpdate]
    [NopMigration("2021/03/04 11:35:09:1647929", "Shop location table")]
    public class SchemaMigration : AutoReversingMigration
    {
        protected IMigrationManager _migrationManager;

        public SchemaMigration(IMigrationManager migrationManager)
        {
            _migrationManager = migrationManager;
        }

        public override void Up()
        {
            _migrationManager.BuildTable<Location>(Create);
        }
    }
}