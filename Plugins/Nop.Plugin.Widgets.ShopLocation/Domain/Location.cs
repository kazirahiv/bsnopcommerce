﻿using Nop.Core;

namespace Nop.Plugin.Widgets.ShopLocation.Domain
{
    public class Location : BaseEntity
    {
        public string ShopId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}
