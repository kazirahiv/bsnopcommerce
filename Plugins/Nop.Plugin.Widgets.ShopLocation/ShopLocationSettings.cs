﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.ShopLocation
{
    class ShopLocationSettings : ISettings
    {
        public bool UseSandbox { get; set; }
        public string Message { get; set; }
    }
}
