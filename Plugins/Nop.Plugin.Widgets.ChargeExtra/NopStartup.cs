﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Nop.Core.Infrastructure;
using Nop.Plugin.Misc.ChargeExtra.Infrastructure;

namespace Nop.Plugin.Misc.ChargeExtra
{
    public class NopStartup : INopStartup
    {

        public void Configure(IApplicationBuilder application)
        {
        }

        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<RazorViewEngineOptions>(options =>
            {
                options.ViewLocationExpanders.Add(new ComponentLocationExpandar());
            });
        }

        public int Order
        {
            get { return int.MaxValue; }

        }
    }
    }
