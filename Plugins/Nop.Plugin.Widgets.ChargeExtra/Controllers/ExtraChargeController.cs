﻿
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Misc.ChargeExtra;
using Nop.Plugin.Misc.ChargeExtra.Services;
using Nop.Services.Configuration;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Misc.ChargeExtra.Controllers
{
    public class ExtraChargeController : BasePluginController
    {
        #region Fields

        private readonly IChargeExtraService _ChargeExtraService;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;

        #endregion

        #region Ctor

        public ExtraChargeController(
            IChargeExtraService chargeExtraService,
            IStoreContext storeContext,
            ISettingService settingService)
        {
            _ChargeExtraService = chargeExtraService;
            _storeContext = storeContext;
            _settingService = settingService;
        }

        #endregion
        public virtual async Task<IActionResult> List()
        {
            //prepare model
            var model = await _ChargeExtraService.GetAllAsync();


            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var chargeExtraSettings = await _settingService.LoadSettingAsync<ChargeExtraSettings>(storeScope);

            return Json(new { 
                Reason = "Extra Charge",
                Charge = chargeExtraSettings.Charge
            });
        }
    }
}