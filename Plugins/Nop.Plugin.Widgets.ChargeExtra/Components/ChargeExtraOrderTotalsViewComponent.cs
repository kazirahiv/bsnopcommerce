﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Misc.ChargeExtra.Models;
using Nop.Services.Orders;
using Nop.Web.Factories;
using Nop.Web.Framework.Components;
using Nop.Plugin.Misc.ChargeExtra.Infrastructure.Mapper.Extensions;
using Nop.Core.Infrastructure.Mapper;
using Nop.Services.Configuration;

namespace Nop.Plugin.Misc.ChargeExtra.Components
{
    public class ChargeExtraOrderTotalsViewComponent : NopViewComponent
    {
        private readonly IShoppingCartModelFactory _shoppingCartModelFactory;
        private readonly IShoppingCartService _shoppingCartService;
        private readonly IStoreContext _storeContext;
        private readonly IWorkContext _workContext;
        private readonly ISettingService _settingService;

        public ChargeExtraOrderTotalsViewComponent(IShoppingCartModelFactory shoppingCartModelFactory,
            IShoppingCartService shoppingCartService,
            IStoreContext storeContext,
            IWorkContext workContext,
            ISettingService settingService)
        {
            _shoppingCartModelFactory = shoppingCartModelFactory;
            _shoppingCartService = shoppingCartService;
            _storeContext = storeContext;
            _workContext = workContext;
            _settingService = settingService;
        }

        /// <returns>A task that represents the asynchronous operation</returns>
        public async Task<IViewComponentResult> InvokeAsync(bool isEditable)
        {
            var cart = await _shoppingCartService.GetShoppingCartAsync(await _workContext.GetCurrentCustomerAsync(), ShoppingCartType.ShoppingCart, (await _storeContext.GetCurrentStoreAsync()).Id);

            var model = await _shoppingCartModelFactory.PrepareOrderTotalsModelAsync(cart, isEditable);

            var summary = AutoMapperConfiguration.Mapper.Map<OrderSummary>(model);

            var charge = await _settingService.LoadSettingAsync<ChargeExtraSettings>();

            summary.ExtraCharge = charge.Charge.ToString("0.00");
            return View("~/Plugins/Misc.ChargeExtra/Views/MiscOrderTotals/Summary.cshtml", summary);
        }
    }
}
