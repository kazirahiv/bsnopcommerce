﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ChargeExtra
{
    class ChargeExtraDefaults
    {
        public static string ConfigurationRouteName => "Misc.ChargeExtra.Configure";
        public static string ChargeExtraSettings => "chargeextrasettings.charge";
    }
}
