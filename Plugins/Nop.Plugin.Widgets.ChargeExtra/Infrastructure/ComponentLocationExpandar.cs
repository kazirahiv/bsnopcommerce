﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ChargeExtra.Infrastructure
{
    class ComponentLocationExpandar : IViewLocationExpander
    {
        private const string THEME_KEY = "nop.themename";

        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {

            if (context.Values.TryGetValue(THEME_KEY, out string theme))
            {
                if (context.ViewName == "Components/OrderTotals/Default")
                {
                    viewLocations = new[] {
                         $"~/Plugins/Misc.ChargeExtra/Views/MiscOrderTotals/Default.cshtml",
                    }
                        .Concat(viewLocations);
                }
            }

            return viewLocations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            //Nothing to do
        }
    }
}
