﻿using Nop.Core.Infrastructure.Mapper;
using AutoMapper;
using Nop.Plugin.Misc.ChargeExtra.Domain;
using Nop.Plugin.Misc.ChargeExtra.Models;
using Nop.Web.Models.ShoppingCart;

namespace Nop.Plugin.Misc.ChargeExtra.Mapper
{
    class MapperConfiguration : Profile, IOrderedMapperProfile
    {

        #region Ctor
        public MapperConfiguration()
        {
            CreateChargeExtraMap();
        }

        #endregion

        #region Utilities

        protected virtual void CreateChargeExtraMap()
        {
            CreateMap<ExtraCharge, PublicChargeExtraModel>();
            CreateMap<PublicChargeExtraModel, ExtraCharge>();
            CreateMap<OrderTotalsModel, OrderSummary>();
        }


        #endregion

        #region Properties

        /// <summary>
        /// Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion
    }
}
