﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Misc.ChargeExtra.Infrastructure
{
    class RouteProvider : IRouteProvider
    {
        public void RegisterRoutes(IEndpointRouteBuilder endpointRouteBuilder)
        {
            endpointRouteBuilder.MapControllerRoute(ChargeExtraDefaults.ConfigurationRouteName, "Plugins/ChargeExtra/Configure",
                new { controller = "ChargeExtra", action = "Configure" });
            endpointRouteBuilder.MapControllerRoute(name: "areaRoute",
                pattern: $"{{area:exists}}/{{controller=ChargeExtra}}/{{action=Index}}/{{id?}}");
        }

        public int Priority => 0;
    }
}
