﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Plugins;

namespace Nop.Plugin.Misc.ChargeExtra
{
    public class ChargeExtraPlugin : BasePlugin, IMiscPlugin
    {
        private readonly IWebHelper _webHelper;
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;

        public bool HideInWidgetList => false;

        public ChargeExtraPlugin(IWebHelper webHelper, ILocalizationService localizationService)
        {
            _webHelper = webHelper;
            _localizationService = localizationService;
        }



        public override string GetConfigurationPageUrl()
        {
            return $"{_webHelper.GetStoreLocation()}Admin/ChargeExtra/Configure";
        }


        public override async Task InstallAsync()
        {
            var settings = new ChargeExtraSettings
            {
                Charge = 0
            };
            await _settingService.SaveSettingAsync(settings);

            await _localizationService.AddLocaleResourceAsync(new Dictionary<string, string>
            {
                ["Plugins.Misc.ChargeExtra.Name"] = "Name",
                ["Plugins.Misc.ChargeExtra.Charge"] = "Charge",
                ["Plugins.Misc.ChargeExtra.Reason"] = "Reason",
            });

            await base.InstallAsync();
        }

        public override async Task UninstallAsync()
        {
            await _localizationService.DeleteLocaleResourcesAsync("Plugins.Misc.ChargeExtra");

            await base.UninstallAsync();
        }
    }
}
