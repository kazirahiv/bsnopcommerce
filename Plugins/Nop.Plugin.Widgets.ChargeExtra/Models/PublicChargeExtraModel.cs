﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ChargeExtra.Models
{
    public record PublicChargeExtraModel : BaseNopEntityModel
    {
        public string Name { get; set; }
        public decimal Charge { get; set; }
        public string Reason { get; set; }
    }
}
