﻿using Nop.Web.Framework.Models;
using Nop.Web.Models.ShoppingCart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ChargeExtra.Models
{
    public record OrderSummary : OrderTotalsModel
    {
        public string ExtraCharge { get; set; }
    }
}
