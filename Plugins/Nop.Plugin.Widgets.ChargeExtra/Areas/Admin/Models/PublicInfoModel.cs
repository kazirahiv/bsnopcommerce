﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models
{
    public record PublicInfoModel : BaseNopModel
    {
        public decimal Charge { get; set; }
    }
}
