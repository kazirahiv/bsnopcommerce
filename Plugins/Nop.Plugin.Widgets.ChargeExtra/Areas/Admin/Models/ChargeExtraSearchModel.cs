﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models
{

    /// <summary>
    /// Represents a location search model
    /// </summary>
    public partial record ChargeExtraSearchModel : BaseSearchModel
    {

        #region Properties

        [NopResourceDisplayName("Admin.Catalog.Manufacturers.List.SearchManufacturerName")]
        public string SearchChargeExtraName { get; set; }

        #endregion
    }
}
