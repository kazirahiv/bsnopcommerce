﻿using Nop.Web.Framework.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models
{

    public partial record ChargeExtraListModel : BasePagedListModel<ChargeExtraModel>
    {
    }
}
