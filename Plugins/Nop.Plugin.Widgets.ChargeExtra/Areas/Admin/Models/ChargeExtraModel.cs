﻿using Nop.Web.Framework.Models;
using Nop.Web.Framework.Mvc.ModelBinding;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models
{
    public record ChargeExtraModel :  BaseNopEntityModel
    {
        [NopResourceDisplayName("Plugins.Misc.ChargeExtra.Name")]
        public string Name { get; set; }
        [NopResourceDisplayName("Plugins.Misc.ChargeExtra.Charge")]
        public decimal Charge { get; set; }
        [NopResourceDisplayName("Plugins.Misc.ChargeExtra.Reason")]
        public string Reason { get; set; }
    }
}
