﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Nop.Services.Security;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;
using Nop.Services.Messages;
using Nop.Plugin.Misc.ChargeExtra.Services;
using Nop.Plugin.Misc.ChargeExtra.Domain;
using System.Linq;
using Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models;
using Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Factories;
using Nop.Web.Framework.Mvc.Filters;
using System;
using Nop.Plugin.Misc.ChargeExtra.Infrastructure.Mapper.Extensions;
using System.Collections.Generic;
using Nop.Core.Domain.Orders;
using Nop.Services.Orders;
using Nop.Core;
using Nop.Services.Configuration;
using Nop.Services.Localization;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Controllers
{
    [Area(AreaNames.Admin)]
    [AutoValidateAntiforgeryToken]
    public class ChargeExtraController : BasePluginController
    {

        #region Fields

        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly IChargeExtraService _ChargeExtraService;
        private readonly ICheckoutAttributeService _checkoutAttributeService;
        private readonly IChargeExtraModelFactory _ChargeExtraModelFactory;
        private readonly IStoreContext _storeContext;
        private readonly ISettingService _settingService;
        private readonly ILocalizationService _localizationService;

        #endregion

        #region Ctor

        public ChargeExtraController(
            INotificationService notificationService,
            IPermissionService permissionService,
            IChargeExtraService ChargeExtraService,
            IChargeExtraModelFactory ChargeExtraModelFactory,
            ICheckoutAttributeService checkoutAttributeService,
            IStoreContext storeContext,
            ISettingService settingService,
            ILocalizationService localizationService)
        {
            _notificationService = notificationService;
            _permissionService = permissionService;
            _ChargeExtraService = ChargeExtraService;
            _ChargeExtraModelFactory = ChargeExtraModelFactory;
            _checkoutAttributeService = checkoutAttributeService;
            _storeContext = storeContext;
            _settingService = settingService;
            _localizationService = localizationService;
        }

        #endregion


        #region List

        public async Task<IActionResult> Configure()
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var chargeExtraSettings = await _settingService.LoadSettingAsync<ChargeExtraSettings>(storeScope);
            var model = new PublicInfoModel
            {
                Charge = chargeExtraSettings.Charge
            };

            return View("~/Plugins/Misc.ChargeExtra/Areas/Admin/Views/Configure.cshtml", model);
        }


        [HttpPost]
        public async Task<IActionResult> Configure(ConfigurationModel model)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = await _storeContext.GetActiveStoreScopeConfigurationAsync();
            var chargeExtraSettings = await _settingService.LoadSettingAsync<ChargeExtraSettings>(storeScope);
            chargeExtraSettings.Charge = model.Charge;
            await _settingService.SaveSettingAsync(chargeExtraSettings, x => x.Charge, storeScope, false);

            //now clear settings cache
            await _settingService.ClearCacheAsync();

            _notificationService.SuccessNotification(await _localizationService.GetResourceAsync("Admin.Plugins.Saved"));
            return await Configure();
        }

        /// <returns>A task that represents the asynchronous operation</returns>


        public async Task<IActionResult> List()
        {
            var model = await _ChargeExtraModelFactory.PrepareChargeExtraSearchModelAsync(new ChargeExtraSearchModel());
            return View("~/Plugins/Misc.ChargeExtra/Areas/Admin/Views/List.cshtml", model);
        }

        [HttpPost]
        public virtual async Task<IActionResult> List(ChargeExtraSearchModel searchModel)
        {
            //prepare model
            var model = await _ChargeExtraModelFactory.PrepareChargeExtraListModelAsync(searchModel);

            return Json(model);
        }

        #endregion


        #region Create / Edit / Delete

        #region Create
        public virtual async Task<IActionResult> Create()
        {
            //prepare model
            var model = await _ChargeExtraModelFactory.PrepareChargeExtraModelAsync(new ChargeExtraModel(), null);

            return View("~/Plugins/Misc.ChargeExtra/Areas/Admin/Views/Create.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        public virtual async Task<IActionResult> Create(ChargeExtraModel model, bool continueEditing)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            if (ModelState.IsValid)
            {

                var charge = model.ToEntity<ExtraCharge>();
                await _ChargeExtraService.InsertChargeAsync(charge);


                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = charge.Id });
            }

            //prepare model
            model = await _ChargeExtraModelFactory.PrepareChargeExtraModelAsync(model, null, true);

            //if we got this far, something failed, redisplay form
            return View("~/Plugins/Misc.ChargeExtra/Areas/Admin/Views/Create.cshtml", model);
        }

        #endregion

        #region Edit

        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Edit(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            //try to get a Charge with the specified id
            var Charge = await _ChargeExtraService.GetByIdAsync(id);
            if (Charge == null)
                return RedirectToAction("List");

            //prepare model
            var model = await _ChargeExtraModelFactory.PrepareChargeExtraModelAsync(null, Charge);

            return View("~/Plugins/Misc.ChargeExtra/Areas/Admin/Views/Edit.cshtml", model);
        }

        [HttpPost, ParameterBasedOnFormName("save-continue", "continueEditing")]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Edit(ChargeExtraModel model, bool continueEditing)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            //try to get a Charge with the specified id
            var manufacturer = await _ChargeExtraService.GetByIdAsync(model.Id);
            if (manufacturer == null)
                return RedirectToAction("List");

            if (ModelState.IsValid)
            {
                manufacturer = model.ToEntity(manufacturer);
                await _ChargeExtraService.UpdateChargeAsync(manufacturer);
                _notificationService.SuccessNotification("Charge Updated !");

                if (!continueEditing)
                    return RedirectToAction("List");

                return RedirectToAction("Edit", new { id = manufacturer.Id });
            }

            //prepare model
            model = await _ChargeExtraModelFactory.PrepareChargeExtraModelAsync(model, manufacturer, true);

            //if we got this far, something failed, redisplay form
            return View("~/Plugins/Misc.ChargeExtra/Areas/Admin/Views/Create.cshtml", model);
        }


        #endregion

        #region Delete


        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> Delete(int id)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            //try to get a Charge with the specified id
            var Charge = await _ChargeExtraService.GetByIdAsync(id);
            if (Charge == null)
                return RedirectToAction("List");

            await _ChargeExtraService.DeleteChargeAsync(id);

            _notificationService.SuccessNotification("Charge Deleted !");

            return RedirectToAction("List");
        }

        [HttpPost]
        /// <returns>A task that represents the asynchronous operation</returns>
        public virtual async Task<IActionResult> DeleteSelected(ICollection<int> selectedIds)
        {
            if (!await _permissionService.AuthorizeAsync(StandardPermissionProvider.ManageManufacturers))
                return AccessDeniedView();

            if (selectedIds != null)
            {
                var Charges = await _ChargeExtraService.GetChargesByIdsAsync(selectedIds.ToArray());
                await _ChargeExtraService.DeleteChargeExtrasAsync(Charges);
            }

            return Json(new { Result = true });
        }

        #endregion

        #endregion
    }
}
