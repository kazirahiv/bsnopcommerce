﻿using Nop.Core.Infrastructure.Mapper;
using AutoMapper;
using Nop.Plugin.Misc.ChargeExtra.Domain;
using Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Infrastructure.Mapper
{
    class AdminMapperConfiguration : Profile, IOrderedMapperProfile
    {

        #region Ctor
        public AdminMapperConfiguration()
        {
            CreateLocationMaps();
        }

        #endregion

        #region Utilities

        protected virtual void CreateLocationMaps()
        {
            CreateMap<ExtraCharge, ChargeExtraModel>();
            CreateMap<ChargeExtraModel, ExtraCharge>();
        }
        #endregion

        #region Properties

        /// <summary>
        /// Order of this mapper implementation
        /// </summary>
        public int Order => 0;

        #endregion
    }
}
