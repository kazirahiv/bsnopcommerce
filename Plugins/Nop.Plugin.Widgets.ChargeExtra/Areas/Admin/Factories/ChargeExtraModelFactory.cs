﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models;
using Nop.Plugin.Misc.ChargeExtra.Domain;
using Nop.Plugin.Misc.ChargeExtra.Infrastructure.Mapper.Extensions;
using Nop.Plugin.Misc.ChargeExtra.Services;
using Nop.Web.Framework.Models.Extensions;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the manufacturer model factory implementation
    /// </summary>
    public partial class ChargeExtraModelFactory : IChargeExtraModelFactory
    {
        #region Fields
        private readonly IChargeExtraService _ChargeExtraService;
        #endregion

        #region Ctor

        public ChargeExtraModelFactory(IChargeExtraService ChargeExtraService)
        {
            _ChargeExtraService = ChargeExtraService;
        }

        #endregion


        #region Methods

        public virtual async Task<ChargeExtraSearchModel> PrepareChargeExtraSearchModelAsync(ChargeExtraSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));
            //prepare page parameters
            searchModel.SetGridPageSize();

            return searchModel;
        }

        public virtual async Task<ChargeExtraListModel> PrepareChargeExtraListModelAsync(ChargeExtraSearchModel searchModel)
        {
            if (searchModel == null)
                throw new ArgumentNullException(nameof(searchModel));

            //get manufacturers
            var ChargeExtras = await _ChargeExtraService.GetAllChargesPagedAsync();

            // prepare grid model
            var model = await new ChargeExtraListModel().PrepareToGridAsync(searchModel, ChargeExtras, () =>
            {
                //fill in model values from the entity
                return ChargeExtras.Select(ChargeExtra =>
                {
                    var chargeExtraModel = ChargeExtra.ToModel<ChargeExtraModel>();
                    return chargeExtraModel;
                }).ToAsyncEnumerable();
            });

            return model;
        }

        /// <summary>
        /// Prepare Charge model
        /// </summary>
        /// <param name="model">Manufacturer model</param>
        /// <param name="manufacturer">Manufacturer</param>
        /// <param name="excludeProperties">Whether to exclude populating of some properties of model</param>
        /// <returns>
        /// A task that represents the asynchronous operation
        /// The task result contains the manufacturer model
        /// </returns>
        public virtual async Task<ChargeExtraModel> PrepareChargeExtraModelAsync(ChargeExtraModel model, ExtraCharge Charge, bool excludeProperties = false)
        {
            if(Charge == null)
            {
                Charge = new ExtraCharge();
            }
            var mapped = Charge.ToModel<ChargeExtraModel>();
            return mapped;
        }

            #endregion
        }
}