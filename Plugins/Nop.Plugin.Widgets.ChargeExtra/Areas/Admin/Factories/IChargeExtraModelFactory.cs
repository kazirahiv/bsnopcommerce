﻿using System.Threading.Tasks;
using Nop.Core.Domain.Catalog;
using Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Models;
using Nop.Plugin.Misc.ChargeExtra.Domain;

namespace Nop.Plugin.Misc.ChargeExtra.Areas.Admin.Factories
{
    /// <summary>
    /// Represents the shop location model factory
    /// </summary>
    public partial interface IChargeExtraModelFactory
    {
        Task<ChargeExtraSearchModel> PrepareChargeExtraSearchModelAsync(ChargeExtraSearchModel searchModel);

        Task<ChargeExtraListModel> PrepareChargeExtraListModelAsync(ChargeExtraSearchModel searchModel);

        Task<ChargeExtraModel> PrepareChargeExtraModelAsync(ChargeExtraModel model,
            ExtraCharge location, bool excludeProperties = false);

    }
}