﻿using Nop.Core;

namespace Nop.Plugin.Misc.ChargeExtra.Domain
{
    public class ExtraCharge : BaseEntity
    {
        public string Name { get; set; }
        public decimal Charge { get; set; }
        public string Reason { get; set; }
    }
}
