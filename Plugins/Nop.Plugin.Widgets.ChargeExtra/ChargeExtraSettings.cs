﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Misc.ChargeExtra
{
    class ChargeExtraSettings : ISettings
    {
        public decimal Charge { get; set; }
    }
}
