﻿using FluentMigrator;
using Nop.Data.Migrations;
using Nop.Plugin.Misc.ChargeExtra.Domain;

namespace Nop.Plugin.Misc.ChargeExtra.Data
{
    [SkipMigrationOnUpdate]
    [NopMigration("2021/03/06 11:35:09:1647929", "Charge Extra table")]
    public class SchemaMigration : AutoReversingMigration
    {
        protected IMigrationManager _migrationManager;

        public SchemaMigration(IMigrationManager migrationManager)
        {
            _migrationManager = migrationManager;
        }

        public override void Up()
        {
            _migrationManager.BuildTable<ExtraCharge>(Create);
        }
    }
}
