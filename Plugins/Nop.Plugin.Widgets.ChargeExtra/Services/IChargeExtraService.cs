﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Plugin.Misc.ChargeExtra.Domain;

namespace Nop.Plugin.Misc.ChargeExtra.Services
{
    /// <summary>
    /// Represents service shipping by weight service
    /// </summary>
    public partial interface IChargeExtraService
    {
        Task<List<ExtraCharge>> GetAllAsync();
        Task InsertChargeAsync(ExtraCharge Charge);
        Task UpdateChargeAsync(ExtraCharge Charge);
        Task<int> DeleteChargeAsync(int id);
        Task DeleteChargeExtrasAsync(IList<ExtraCharge> Charges);
        Task<ExtraCharge> GetByIdAsync(int id);
        Task<IList<ExtraCharge>> GetChargesByIdsAsync(int[] ChargeIds);
        Task<IPagedList<ExtraCharge>> GetAllChargesPagedAsync(int pageIndex = 0, int pageSize = int.MaxValue);
    }
}
