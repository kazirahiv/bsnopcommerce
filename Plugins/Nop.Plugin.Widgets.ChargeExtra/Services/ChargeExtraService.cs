﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Nop.Core;
using Nop.Data;
using Nop.Plugin.Misc.ChargeExtra.Domain;

namespace Nop.Plugin.Misc.ChargeExtra.Services
{
    /// <summary>
    /// Represents service shipping by weight service implementation
    /// </summary>
    public partial class ChargeExtraService : IChargeExtraService
    {

        #region Fields

        private readonly IRepository<ExtraCharge> _ChargeRepository;

        #endregion

        #region Ctor

        public ChargeExtraService(IRepository<ExtraCharge> ChargeRepository)
        {
            _ChargeRepository = ChargeRepository;
        }

        #endregion

        #region Methods

        public virtual async Task<List<ExtraCharge>> GetAllAsync()
        {
            var result = await _ChargeRepository.GetAllAsync(query =>
            {
                return from sbw in query select sbw;
            });

            var records = result.ToList();

            return records;
        }

        public virtual async Task<IPagedList<ExtraCharge>> GetAllChargesPagedAsync(int pageIndex = 0,
            int pageSize = int.MaxValue)
        {

            return await _ChargeRepository.GetAllPagedAsync(async query =>
            {
                return query.OrderBy(s => s.Id);
            }, pageIndex, pageSize);
        }
        

        public virtual async Task<IList<ExtraCharge>> GetChargesByIdsAsync(int[] ChargeIds)
        {
            return await _ChargeRepository.GetByIdsAsync(ChargeIds, includeDeleted: false);
        }

        public virtual async Task DeleteChargeExtrasAsync(IList<ExtraCharge> Charges)
        {
            await _ChargeRepository.DeleteAsync(Charges);
        }

        public virtual async Task<ExtraCharge> GetByIdAsync(int id)
        {
            var record = await _ChargeRepository.GetByIdAsync(id);

            return record;
        }

        public virtual async Task InsertChargeAsync(ExtraCharge Charge)
        {
            await _ChargeRepository.InsertAsync(Charge, false);
        }

        public virtual async Task UpdateChargeAsync(ExtraCharge Charge)
        {
            await _ChargeRepository.UpdateAsync(Charge);
        }

        public virtual async Task<int> DeleteChargeAsync(int id)
        {
            var result = await _ChargeRepository.DeleteAsync(s => s.Id == id);
            return result;
        }

        #endregion
    }
}
